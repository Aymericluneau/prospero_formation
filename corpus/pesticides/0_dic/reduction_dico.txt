Rhétorique scientifique + Raisonnement statistique
Formes juridiques + Processus de normalisation
Décisions univoques et souveraines + Logique_décision_action
Déploiement de conséquences + Gestion des risques + Logique sanitaire
Contrôle et vérification + Enquête-Investigation + Mode de preuve expérimentale
Discours_environnementaliste + Rhétorique_écologique
Contamination + Logique de seuil + Toxicologie + Approches Epidémiologique
Logique-d-alarme + Attention/vigilance + Inquiétudes
Etats critiques et défaillances + Crises_et_Catastrophes
Modes de protestation + Régime de controverses + Critique évaluative
Emplois et Conditions de travail
Modes de dénonciations + Régime de polémique + Logique judiciaire + Figures de réparation


#######
Qualité

Dangerosité 
Important-Essentiel
Accusation-Critique + Fausseté + Défailance-Etat-critique
Fragilité
Arrêt Clôture + Adhésion Satisfaction
Assurance-Certitude-Fiabilité + Réalité/vérité + Identification Observation
Relativisation + Incertitude

#####
Marqueur

Démonstraton
Citation
Contradiction-réfutation
Irréversibilité

######
Epreuve

Epistémique [marqueur]  + Juger-Estimer
Exiger-Réclamer + Résister + Mobiliser/Rassembler
Accuser 
Dévoiler/Découvrir  + Prouvver-Valider + Argumenter Expliciter Justifier
Calculer/Définir/Evaluer  + Analyser/Observer + Surveiller/Vérifier
Lancer-une-alerte
Connaître/Savoir (au passé) + Connaître/Savoir (au présent)
Défendre-Soutenir + Contredire/Démentire
Avouer/Reconnaître
Ignorer/Oublier

######
Collection et EF
[Collection] 
*PLANTES-QU-ON-MANGE*
*NOURRITURE-BOISSON* [BOISSONS]	
MALADIES-DES-PLANTES*
*AGRICULTURE@ + APICULTEURS@ + AGRICULTURE* (exploitants, pratiques et techniques)
*ETAT-CENTRAL@ + ETAT-DECENTRALISE@
*FIRMES-AGROCHIMIQUES@ + ASSO-PROF-PHYTO-AGRO*
*EUR-INSTITUTIONS@ + EUR-UNION@
*LES-COMMUNSE@ + AMF@
*PESTICIDES@ + PRODUITS-PHYTO@ + Roundup@ + NEONICOTINOIDES@ + HERBICIDES@ + CHLORDECONE@ + SDHI@ + INSECTICIDES@ + LINDANE@ + PARAQUAT@ + AGENT-ORANGE@
*LE-CANCER@ + Santé@
PARLEMENT@
VICTIMES@
*ASSOCIATIONS@ + FNE@ + COLLECTIFS-MOUVEMENTS-SOCIAUX* + LPO@ + WWF@
*LA-PLANETE@ + EAU@
*CHERCHEURS@ + SCIENCES
*INRA@ + CEMAGREF@
ANSES@
GRANDE DISTRIBUTION@
RIVERAINS@
ENFANT@
*CIRC@ + OMS@
EFSA@
*TRAVAILLEURS@ + FNATH@ + CHSCT@
*INSECTES@ + BESTIAIRE* + POLLINISATEUR@
*SYNDICATS@ + CGT@
CORPS-MEDICAL@
ACADEMIES-SCIENTIFIQUES@
CONFEDERATION-PAYSANNE@ + SYNDICATS-AGRICOLES* = UNAF@
PHYTOVICTIMES@
GENERATIONS-FUTURES@

