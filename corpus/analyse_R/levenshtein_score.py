elif which_score == "2":
      n_texts = len(df["Chemin"])
      for x, i in enumerate(df["Chemin"]):
          if i not in list_textes_exclus:
              with open(i,"r", newline='', encoding = 'mbcs') as txt_file:
                  text_a = txt_file.read()
              text_to_compare = df.loc[x+1:, "Chemin"]

          for y, j in enumerate(text_to_compare):
              #print(y+1, len(text_to_compare))
              if y+1 == len(text_to_compare):
                  print(f'le texte {x+1} est comparé au texte {x+y+2}')
              if j not in list_textes_exclus:
                  with open(j,"r", newline='', encoding = 'mbcs') as txt_file2:
                      text_b = txt_file2.read()
                  score = lev.ratio(text_a, text_b)
                  if score > 0.95:
                      list_text_a2.append(i)
                      list_textes_exclus.append(j)
                      list_score2.append(score)
                  else:
                      list_text_a.append(i)
                      list_text_b.append(j)
                      list_score.append(score)

      data = {"Text_a" : list_text_a, "Text_b" : list_text_b, "Levenshtein" : list_score}
      data2 = {"Text_a" : list_text_a2, "Text_b" : list_textes_exclus, "Levenshtein" : list_score2}
      df2 = pandas.DataFrame(data)
      df3 = pandas.DataFrame(data2)
      df2.to_csv("Liste_score_lev.csv", sep = '\t', index = False, encoding = 'utf-8')
      df3.to_csv("Liste_score_lev2.csv", sep = '\t', index = False, encoding = 'utf-8')
