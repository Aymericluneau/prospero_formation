#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata
import numpy
import shutil


PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)

df = pandas.read_csv("Liste_fichiers_PRC.csv", sep = '\t')

liste_folder = []
liste_txt = []
liste_ctx = []

for i in df["Chemin"]:
    chemin_folder = i.split("\\")[3]
    liste_folder.append(chemin_folder)
    liste_txt.append(i)
    liste_ctx.append(i.replace(".txt", ".ctx"))


dossier_parent = os.path.dirname(PATH)
liste_folder_text = []

for i in liste_folder:
    dossier_lu = f'{dossier_parent}\\{i}'
    print(dossier_lu)
    new_repertoire = f'{dossier_parent}\\{i}\\old_text'
    try:
        os.makedirs(new_repertoire)
    except OSError:
        if not os.path.isdir(new_repertoire):
            Raise
    for element in os.listdir(dossier_lu) : # Pour chaque élément contenu dans le dossier parent
        element_path = f'{dossier_parent}\\{i}\\{element}'
        if element_path.endswith(".txt"):
            if element_path in liste_txt :
                print(element_path)
            else:
                shutil.move(element_path, new_repertoire)
                print("FALSE")
        elif element_path.endswith(".ctx"):
            if element_path in liste_ctx:
                print(element_path)
            else:
                shutil.move(element_path, new_repertoire)
                print("FALSE")




            # '
            # element_ctx = f'{dossier_parent}\\{i}\\{element.replace(".txt", ".ctx")}'





# for element in os.listdir(dossier_parent) : # Pour chaque élément contenu dans le dossier parent
#     adresse_element = f"{dossier_parent}\\{element}" #on recompose l'adresse de chque objet du dossier
#     if adresse_element.endswith(".prc"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
#         liste_prc.append(adresse_element)
#     if os.path.isdir(adresse_element) : # si l'objet est un dossier, alors...
#         for files in os.listdir(adresse_element): #on regarde chaque objet du dossier
#             adresse_files = f"{adresse_element}\\{files}"
#             if adresse_files.endswith(".prc"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
#                 liste_prc.append(adresse_files)
