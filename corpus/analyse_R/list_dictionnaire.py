#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata

PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)

def save_dictionnaire(list_name_rep, list_type_representant):
    data = {'Representant' : list_name_rep, #On assemble les trois lise (collection, sous-collection, représentant) dans un dataframe
    'Type_representant' : list_type_representant,
    }
    df = pandas.DataFrame (data, columns = ['Representant', 'Type_representant'], index = None)
    df_output = df.to_csv("liste_dict_base.csv", sep = '\t', index = False, encoding = "utf-8") #on écrit les résultats dans le fichier

# liste_collections = "C:/Corpus/medialab/pesticides/0_dic/santenv.COL" #nom du fichier contenant les collectioncs
# liste_fictions = "C:/Corpus/medialab/pesticides/0_dic/santenv.fic"
# liste_categories = "C:/Corpus/medialab/pesticides/0_dic/santenv.CAT"



path_base = "C:/Corpus/medialab/0_dic/dic_elementaires"
files = ['fr_epreu.dic', 'fr_etre.dic', 'fr_marqu.dic', 'fr_quali.dic']
list_file = [f for f in os.listdir(path_base) if f.endswith(".dic") and f in files]
print(list_file)

list_name_rep = []
list_type_representant =[]

for file in list_file:
    path_file = f"{path_base}/{file}"
    with open(path_file,"r", newline='', encoding = 'latin-1') as dic_file: #on ouvre le fichier
        read_dic = dic_file.readlines()
        for line in read_dic:
            list_name_rep.append(line.replace(" \'", "\'").replace("\' ", "\'").replace('\"', "").strip())
            if "fr_etre" in file:
                list_type_representant.append("entite")
            elif "fr_epreu" in file :
                list_type_representant.append("epreuve")
            elif "fr_marqu" in file :
                list_type_representant.append("marqueur")
            elif "fr_quali" in file :
                list_type_representant.append("qualite")

save_dictionnaire(list_name_rep, list_type_representant)
