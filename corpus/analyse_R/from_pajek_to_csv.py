#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata

PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)

#Decoupe collection:

def create_csv(element):

    year = element.split("_")[0]

    with open(element,"r", newline='', encoding = 'mbcs') as pajek_file: #on ouvre le colhier CTX
            read_pajek = pajek_file.read()
    vertice_arc = read_pajek.split("*Arcs\r\n")
    vertice = vertice_arc[0].split("\r\n")
    arcs = vertice_arc[1].split("\r\n")
    dict_nodes = {}

    for v in vertice[1:-1]:
        key = v.split('\"')[0].strip()
        dict_nodes[key] = v.split('\"')[1].strip()
        if dict_nodes[key] not in list_nodes_name :
            list_nodes_name.append(dict_nodes[key])
        else:
            pass

    for a in arcs[0:-1]:
        id_from = a.split(" ")[0].strip()
        list_from_name.append(dict_nodes[id_from])
        id_to = a.split(" ")[1].strip()
        list_to_name.append(dict_nodes[id_to])
        weight = a.split(" ")[2]
        list_weight_edge.append(weight)
        list_year.append(year)

    data_nodes = {'name' : list_nodes_name}
    dfn = pandas.DataFrame (data_nodes, columns = ['name'], index = None)
    nodes_output = dfn.to_csv(f"media_ennemis_nodes.csv", sep = '\t', index = False) #on écrit les résultats dans le fichier
    print(len(list_to_name), len(list_from_name), len(list_weight_edge), len(list_year))
    data_edges = {'from' : list_from_name,
    'to' : list_to_name,
    'weight' : list_weight_edge,
    'year' : list_year}
    dfe = pandas.DataFrame (data_edges, columns = ['from', 'to', 'weight', 'year'], index = None)
    edges_output = dfe.to_csv(f"media_ennemis_edges.csv", sep = '\t', index = False) #on écrit les résultats dans le fichier


list_from_name = []
list_to_name = []
list_weight_edge = []
list_nodes_name = []
list_year = []

for element in os.listdir(PATH) : # Pour chaque élément contenu dans le dossier
    if element.endswith(".net"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
        print(element)
        create_csv(element)
