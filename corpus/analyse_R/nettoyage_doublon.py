#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata
import numpy

PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)


def clean_duplicate_titre(df2, df):
    df3= pandas.DataFrame(columns=("Chemin", "Nom", "Titre", "Auteur", "Date", "Support", "Type_support", "Titre_sans_espaces"))
    #print(df3)
    for i in df2["Titre_sans_espaces"]:
        newdf = df[df.Nom == i]
        frames = [df3, newdf.head(1)]
        df3 = pandas.concat(frames, ignore_index = True, sort = True)
    print(df3)
    df_output = df3.to_csv("Liste_fichiers_PRC2.csv", sep = '\t', index = False, encoding = 'utf-8')
    clean_duplicate_titre(df3)
    create_prc(df4)

def create_prc(df4):
    dossier_parent = os.path.dirname(PATH)
    liste_prc = []
    for element in os.listdir(dossier_parent) : # Pour chaque élément contenu dans le dossier parent
        adresse_element = f"{dossier_parent}\\{element}" #on recompose l'adresse de chque objet du dossier
        if adresse_element.endswith(".prc"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
            liste_prc.append(adresse_element)
        if os.path.isdir(adresse_element) : # si l'objet est un dossier, alors...
            for files in os.listdir(adresse_element): #on regarde chaque objet du dossier
                adresse_files = f"{adresse_element}\\{files}"
                if adresse_files.endswith(".prc"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
                    liste_prc.append(adresse_files)
    if len(liste_prc) > 1:
        print(f"Il existe plusieurs fichiers .prc, lequel voulez-vous utilisez ?")
        for x, i in enumerate(liste_prc):
            print(f"[{x+1}] {i}")
        which_file = input("Entrez le nombre correspondant au fichier souhaité : ")
        print(f" Le fichier choisi est le {which_file}")
        prc_to_read = liste_prc[int(which_file) - 1]
    else:
        prc_to_read = liste_prc[0]
    with open(prc_to_read,"r", newline='', encoding = 'mbcs') as prc_file:
        read_prc = prc_file.readlines()
    with open("cleaned_prc.prc","w", newline='', encoding = 'mbcs') as cleaned_prc:
        for i in read_prc[0:6]:
            cleaned_prc.write(i)
        for i in df4['Chemin']:
            cleaned_prc.write(f'{i}\n')
        cleaned_prc.write('ENDFILE')


#Fichier de départ
liste_fichiers = "Liste_fichiers_PRC.csv" #nom du fichier csv contenant la liste des textes basé sur le PRC

#try :
find_liste_fichier = any(liste_fichiers in file for file in os.listdir(PATH))
if find_liste_fichier == True:
    df = pandas.read_csv(liste_fichiers, sep = '\t') # df as dataframe
    df['Titre_sans_espaces'] = df.Titre.str.lower().replace('[\s]', '', regex=True)
    df2 = df.groupby(['Titre_sans_espaces']).size().reset_index(name='counts')
    print(len(df2))
    df_output = df2.to_csv("Liste_fichiers_PRC2.csv", sep = '\t', index = False, encoding = 'utf-8')
    clean_duplicate_Nom(df2, df)
    #measure_distance(df)
else:
    print("Il n'y a pas le fichier")
