#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata

PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)

#Decoupe collection:





def save_dictionnaire():
    print(list_name_collection)
    data = {'Representant' : list_name_rep, #On assemble les trois lise (collection, sous-collection, représentant) dans un dataframe
    'Type_representant' : list_type_representant,
    'Sous_classe' : list_name_type,
    'Classe' : list_name_collection,
    'Type_classe' : list_type_dictionnaire}
    df = pandas.DataFrame (data, columns = ['Representant', 'Type_representant', 'Sous_classe', 'Classe', "Type_classe"], index = None)
    df_output = df.to_csv("liste_collections.csv", sep = '\t', index = False, encoding = "latin-1") #on écrit les résultats dans le fichier

def decoupe_type_categorie(list_categories, type_dictionnaire):
    for cat in list_categories: # pour chaque collection récupérée à l'aide de la fonction decoupe_collection
        list_name_cat = cat.split("\r\n") #on divise la collection au niveau de chaque ligne
        type_cat = list_name_cat[0].replace("*", "").lower().strip()
        name_cat = list_name_cat[1].strip() #on enlève les blancs et.c et on récupère le nom de la collection que l'on impute à list_name_collection
        #list_type = cat.split("END") #Pour récuperer les sous-collections on divise chaque collection au niveau des "END" (dans le fichier "END" indique la fin d'une sous collection)
        #print(type_cat, name_cat)
        #print(list_name_cat)
        for sous_cat in list_name_cat[2:len(list_name_cat)-1]:
             #print(sous_cat)
             list_name_rep.append(sous_cat.replace(" \'", "\'").replace("\' ", "\'").replace('\"', "").strip()) #on ajoute le nom du représentant à la liste des représentants
             list_type_representant.append(type_cat) #type du représentant : entité, épreuve, qualité ou marqueurs
             list_name_type.append("NA") #Pour les catégories il n'y a pas de sous-classe
             list_name_collection.append(name_cat) #on ajoute le nom de la catégorie à la liste des catégories
             list_type_dictionnaire.append(type_dictionnaire)



def decoupe_categorie(read_dic, type_dictionnaire):
    read_cat = read_dic.replace("ENDFILE\r\n", "")
    liste_categories = read_cat.split("cat0001\r\n")
    liste_categories = liste_categories[1].split("END\r\nENDCAT\r\n") # Découpe le dictionnaire en classe (collection, être-fictif) et crée une liste
    liste_categories = liste_categories[0:len(liste_categories)-1]
    #print("list: ", liste_categories[-1])
    decoupe_type_categorie(liste_categories, type_dictionnaire)




def decoupe_type_collection(list_collection, type_dictionnaire, type_representant):

    for col in list_collection: # pour chaque collection récupérée à l'aide de la fonction decoupe_collection
        list_name_col = col.split("\n") #on divise la collection au niveau de chaque ligne
        name_col = list_name_col[2].strip() #on enlève les blancs et.c et on récupère le nom de la collection que l'on impute à list_name_collection
        list_type = col.split("END") #Pour récuperer les sous-collections on divise chaque collection au niveau des "END" (dans le fichier "END" indique la fin d'une sous collection)
        if type_dictionnaire == "Collection":
            for type_col in list_type : #Pour chaque sous collection
                list_representant = type_col.split("\n") #On divise la sous collection au niveau des lignes
                search_asterisque = any("*" in x for x in list_representant) #On vérifique dans la sous-collection s'il y a le nom de la collection (contient normalement un asterisque) ex: VILLES*
                #print(search_asterisque)
                if search_asterisque == True: #S'il y a le nom de la collection, alors le nom de la sous-collection se trouve en 4e position (3+1)
                    if list_representant[3].strip() == "...":  #si le nom de la sous collection  est trois point, le nouveau nom de la sous collection devient le [nom de la collection en miniscule]_generique : exemple: villes_generique
                        name_type = f'{str.lower(name_col.strip("*"))}_generique'
                    else:
                        name_type = list_representant[3].strip()
                    for rep in list_representant[4:len(list_representant)-1]: #pour chaque ligne de list_representant située en la 5e et la denière position
                        #print(rep.strip())
                        list_name_rep.append(rep.replace(" \'", "\'").replace("\' ", "\'").replace('\"', "").strip()) #on ajoute le nom du représentant à la liste des représentants
                        list_name_type.append(name_type) #on ajoute le nom de la souss-collection à la liste des souss_collections
                        list_name_collection.append(name_col) #on ajoute le nom de la collection à la liste des collections
                        list_type_dictionnaire.append(type_dictionnaire)
                        list_type_representant.append(type_representant)
                else: #S'il n'y a pas le nom de la Collection, alors le nom de la sous-collection se trouve en 2e position (1+1)
                    if list_representant[1].strip() == "...":
                        name_type = f'{str.lower(name_col.strip("*"))}_generique'
                    else:
                        name_type = list_representant[1].strip()
                    #print(name_type.strip())
                    for rep in list_representant[2:len(list_representant)-1]:
                        #print(rep.strip())
                        list_name_rep.append(rep.strip()) #on ajoute le nom du représentant à la liste des représentants
                        list_name_type.append(name_type) #on ajoute le nom de la souss-collection à la liste des souss_collections
                        list_name_collection.append(name_col) #on ajoute le nom de la collection à la liste des collections
                        list_type_dictionnaire.append(type_dictionnaire)
                        list_type_representant.append(type_representant)
        elif type_dictionnaire == "Fiction":
            for type_col in list_type : #Pour chaque sous collection
                list_representant = type_col.split("\n") #On divise la sous collection au niveau des lignes
                search_arobase = any("@" in x for x in list_representant) #On vérifique dans la sous-collection s'il y a le nom de la collection (contient normalement un asterisque) ex: VILLES*
                #print(search_asterisque)
                if search_arobase == True: #S'il y a le nom de la collection, alors le nom de la sous-collection se trouve en 4e position (3+1)
                    if list_representant[3].strip() == "...":  #si le nom de la sous collection  est trois point, le nouveau nom de la sous collection devient le [nom de la collection en miniscule]_generique : exemple: villes_generique
                        name_type = f'{str.lower(name_col.strip("@"))}_generique'
                    else:
                        name_type = list_representant[3].strip()
                    for rep in list_representant[4:len(list_representant)-1]: #pour chaque ligne de list_representant située en la 5e et la denière position
                        #print(rep.strip())
                        list_name_rep.append(rep.replace(" \'", "\'").replace("\' ", "\'").replace('\"', "").strip()) #on ajoute le nom du représentant à la liste des représentants
                        list_name_type.append(name_type) #on ajoute le nom de la souss-collection à la liste des souss_collections
                        list_name_collection.append(name_col) #on ajoute le nom de la collection à la liste des collections
                        list_type_dictionnaire.append(type_dictionnaire)
                        list_type_representant.append(type_representant)
                else: #S'il n'y a pas le nom de la Collection, alors le nom de la sous-collection se trouve en 2e position (1+1)
                    if list_representant[1].strip() == "...":
                        name_type = f'{str.lower(name_col.strip("@"))}_generique'
                    else:
                        name_type = list_representant[1].strip()
                    #print(name_type.strip())
                    for rep in list_representant[2:len(list_representant)-1]:
                        #print(rep.strip())
                        list_name_rep.append(rep.replace(" \'", "\'").replace("\' ", "\'").replace('\"', "").strip()) #on ajoute le nom du représentant à la liste des représentants
                        list_name_type.append(name_type) #on ajoute le nom de la souss-collection à la liste des souss_collections
                        list_name_collection.append(name_col) #on ajoute le nom de la collection à la liste des collections
                        list_type_dictionnaire.append(type_dictionnaire)
                        list_type_representant.append(type_representant)

def decoupe_collection(read_dic, type_dictionnaire, type_representant):
    list_collection = read_dic.split("ENDFICTION") # Découpe le dictionnaire en classe (collection, être-fictif) et crée une liste
    decoupe_type_collection(list_collection, type_dictionnaire, type_representant) #lance la fonction decoupe_type



# liste_collections = "C:/Corpus/medialab/pesticides/0_dic/santenv.COL" #nom du fichier contenant les collectioncs
# liste_fictions = "C:/Corpus/medialab/pesticides/0_dic/santenv.fic"
# liste_categories = "C:/Corpus/medialab/pesticides/0_dic/santenv.CAT"

list_dict = {"liste_collections" : "C:/Corpus/medialab/0_dic/Collections/Coll_pesti_medialab_nettoyé.col", #nom du fichier contenant les collectioncs
"liste_fictions" : "C:/Corpus/medialab/0_dic/Etre_fictif/EF_pesti_medialab.fic",
"liste_categories" : "C:/Corpus/medialab/0_dic/Categories/Cat_pesti_medialab.cat"
}

list_type_dictionnaire = []
list_type_representant = []
list_name_collection = [] #liste pour récupérer les noms de chaque collection (créées par l'utilisateur.trice)
list_name_type = [] #liste pour récupérer les noms de chaque sous-collection (créées par l'utilisateur.trice)
list_name_rep = [] ##liste pour récupérer les noms des représentants (entité retrouvé dans les textes)

for key_file in list_dict:
    print(key_file)
    fichier_dict = list_dict[key_file]
    with open(fichier_dict,"r", newline='', encoding = 'latin-1') as dic_file: #on ouvre le fichier
        read_dic = dic_file.read()
    if key_file == "liste_collections":
        type_dictionnaire = "Collection"
        type_representant = "entite"
        decoupe_collection(read_dic, type_dictionnaire, type_representant) # lance la fonction decoupe_collection avec read_dic
    elif key_file == "liste_fictions":
        type_dictionnaire = "Fiction"
        type_representant = "entite"
        decoupe_collection(read_dic, type_dictionnaire, type_representant) # lance la fonction decoupe_collection avec read_dic
    else:
        type_dictionnaire = "Categories"
        decoupe_categorie(read_dic, type_dictionnaire) #lance la fonction decoupe_type



save_dictionnaire()
