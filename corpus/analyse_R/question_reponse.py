#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata

PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)

#Fonction pour récupérer les data in CTX file
def get_CTX_data(liste_fichiers):
    liste_auteur = []
    liste_destinataire = []
    liste_titre = []
    liste_date = []
    liste_type = []
    liste_support = []
    lieu_emission = []
    parti_politique = []
    for element in os.listdir(PATH) : # Pour chaque élément contenu dans le dossier parent
        adresse_element = f"{PATH}\\{element}" #on recompose l'adresse de chque objet du dossier
        if adresse_element.endswith(".ctx") or adresse_element.endswith(".CTX"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
            with open(adresse_element,"r", newline='', encoding = 'mbcs') as ctx_file: #on ouvre le fichier CTX
                read_ctx = ctx_file.read()
            titre = read_ctx.split("\n")[1].strip() #On récupère le titre
            auteur = read_ctx.split("\n")[2].strip() #L'auteur
            destinataire = read_ctx.split("\n")[4].strip() #Le destinataire
            date = read_ctx.split("\n")[5].strip() #Et la date
            support = read_ctx.split("\n")[6].strip()
            type_support = read_ctx.split("\n")[7].strip()
            lieu = read_ctx.split("\n")[10].strip()
            partis = read_ctx.split("\n")[11].strip()
            liste_titre.append(titre)
            liste_auteur.append(auteur)
            liste_destinataire.append(destinataire)
            liste_date.append(date)
            liste_type.append(type_support)
            liste_support.append(support)
            lieu_emission.append(lieu)
            parti_politique.append(partis)
    #Création de nouvelles colonnes
    data = {'Auteur' : liste_auteur,
    'Destinataire' : liste_destinataire,
    'Titre' : liste_titre,
    'Date' : liste_date,
    'Support' : liste_support,
    'Type_support' : liste_type,
    'Lieu_emission' : lieu_emission,
    'Parti_politique' : parti_politique}
    df = pandas.DataFrame (data)
    print(df)
    df_output = df.to_csv(liste_fichiers, sep = '\t', index = False) #on écrit les résultats dans le fichier




#Fichier de départ
liste_fichiers = "C:/Corpus/pollinisation/analyse_R/Listes_questions_reponses.csv" #nom du fichier csv contenant la liste des textes basé sur le PRC
get_CTX_data(liste_fichiers)
