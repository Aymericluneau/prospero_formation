import os
from unidecode import unidecode
import re
import string
import codecs
import Levenshtein as lev
import numpy

def jaccard_similarity(text_a, text_b):
    a = set(text_a.split())
    b = set(text_b.split())
    c = a.intersection(b)
    jaccard_score = float(len(c)) / (len(a) + len(b) - len(c))
    return jaccard_score

text_a = """La météo folle pousse les agriculteurs à bout
.
Les sols sont secs; les plantes ont soif. Le grain pourrait ne pas être assez nourri et je constate, à certains endroits, de mauvaises levées des betteraves et des chicorées sucrières. S'il pleut en suffisance, cela pourrait toutefois être rattrapable." Marianne Streel, agricultrice dans le Namurois, endure comme tant d'autres cultivateurs wallons la sécheresse qui frappe le pays depuis onze mois.

  Nous l'avions rencontrée il y a près d'un an, alors que la météo était à l'inverse de ce que nous connaissons maintenant : de la pluie, de la pluie et encore de la pluie en juin, assortie, pour couronner le tout, d'averses de grêle. "En 25 ans de métier, c'était mon rendement le plus bas. Les cultures ont été attaquées par les maladies et les insectes ravageurs. Et la qualité n'y était pas, sans compter que les prix étaient faibles", dit Marianne Streel.

  Coupables et victimes
  .

  Les tuiles météorologiques s'enchaînent donc. L'agricultrice reste philosophe : "Nous travaillons avec du vivant et on doit faire avec." Réaliste aussi : "On subit les changements climatiques. C'est un stress qui s'ajoute à d'autres. Les agriculteurs en sont en partie responsables (en Wallonie, le secteur émet 11,2 % des gaz à effet de serre, NdlR). Dans nos exploitations, nous essayons d'aller vers plus de durabilité grâce aux nouvelles technologies, à la gestion de l'azote, à la production d'énergie verte ou l'utilisation de la juste dose de pesticides. Mais nous avons besoin que la recherche publique nous aide avec une sélection d'espèces plus résistantes, plus adaptées."
  A la Fédération wallonne de l'agriculture (FWA), le plus important syndicat agricole du Sud du pays, on étudie le phénomène des changements climatiques, son impact sur l'agriculture et on cherche des solutions.

  L'herbe est rare
  .

  "Les agriculteurs sont inquiets. Cette saison, l'herbe n'a pas assez poussé. En Ardenne, on observe des volumes de la première coupe en baisse de 20 %. En Famenne, cela peut chuter de moitié. Si le fourrage est riche en sucre, il est particulièrement pauvre en protéines, à cause de la sécheresse. Les éleveurs vont devoir acheter des compléments alimentaires (soja, tourteaux de colza) et du fourrage pour leur bétail. Certains constituent déjà des dossiers en vue d'une indemnisation. Pour la deuxième coupe qui débute ces jours-ci, s'il se met à pleuvoir, on ne réglera pas le problème mais on sauvera les meubles", explique le chargé d'études Christian Hick.
  Afin de limiter un peu la casse, la FWA a introduit auprès du ministre wallon de l'Agriculture une demande d'autorisation de fauchage précoce des bandes herbagères, imposées par l'Union européenne afin de préserver la biodiversité. "René Collin nous a répondu qu'il devait interroger l'Europe et obtenir son accord", indique Christian Hick.
  "Concernant les conséquences de la sécheresse sur les grandes cultures, aucun constat ne peut être posé avant la récolte. On sait toutefois que la saison n'a pas démarré dans les meilleures conditions", signale le chargé d'études.

  Maladie venue d'ailleurs et insectes ravageurs
  .

  Depuis des années, la FWA conseille les agriculteurs sur la meilleure manière de faire face aux situations météorologiques difficiles, qui pourraient devenir la norme, et leurs corollaires : apparition depuis onze ans, sous nos latitudes (elle vient d'Afrique), de la maladie de la langue bleue qui peut tuer les ruminants, augmentation du nombre d'insectes ravageurs, des mauvaises herbes, besoins en eau accrus, faibles rendements agricoles, etc. "Des réponses existent comme la recherche de variétés de plantes plus résistantes (sans qu'elles soient des OGM), opter pour des espèces végétales en prairie moins gourmandes en eau et plus résilientes. Il y a aussi le travail fait par des producteurs/vendeurs de semences afin que ces nouvelles variétés soient disponibles pour les agriculteurs", conclut Christian Hick.
  Déjà onze mois de sécheresse en Belgique. Pour les agriculteurs, c'est la catastrophe : le fourrage est pauvre et le rendement des cultures pourrait être très mauvais.
"""

text_b = """La météo folle pousse les agriculteurs à bout, la recherche va-t-elle offrir une solution?
.

En Wallonie comme ailleurs, les agriculteurs s'inquiètent des dérèglements climatiques. La recherche agricole est mise à contribution comme piste de solution. Les indemnisations pour calamités devraient se multiplier. Les assureurs pourraient décider de réduire leur offre aux agriculteurs.

  Les sols sont secs; les plantes ont soif. Le grain pourrait ne pas être assez nourri et je constate, à certains endroits, de mauvaises levées des betteraves et des chicorées sucrières. S'il pleut en suffisance, cela pourrait toutefois être rattrapable."Marianne Streel, agricultrice dans le Namurois, endure comme tant d'autres cultivateurs wallons la sécheresse qui frappe le pays depuis onze mois.

  Nous l'avions rencontrée il y a près d'un an, alors que la météo était à l'inverse de ce que nous connaissons maintenant : de la pluie, de la pluie et encore de la pluie en juin, assortie, pour couronner le tout, d'averses de grêle. "En 25 ans de métier, c'était mon rendement le plus bas. Les cultures ont été attaquées par les maladies et les insectes ravageurs. Et la qualité n'y était pas, sans compter que les prix étaient faibles", dit Marianne Streel.

  Coupables et victimes
  .

  Les tuiles météorologiques s'enchaînent donc. L'agricultrice reste philosophe : "Nous travaillons avec du vivant et on doit faire avec." Réaliste aussi : "On subit les changements climatiques. C'est un stress qui s'ajoute à d'autres. Les agriculteurs en sont en partie responsables (en Wallonie, le secteur émet 11,2 % des gaz à effet de serre, NdlR).
  Dans nos exploitations, nous essayons d'aller vers plus de durabilité grâce aux nouvelles technologies, à la gestion de l'azote, à la production d'énergie verte ou l'utilisation de la juste dose de pesticides. Mais nous avons besoin que la recherche publique nous aide avec une sélection d'espèces plus résistantes, plus adaptées."
  A la Fédération wallonne de l'agriculture (FWA), le plus important syndicat agricole du Sud du pays, on étudie le phénomène des changements climatiques, son impact sur l'agriculture et on cherche des solutions.

  L'herbe est rare
  .

  "Les agriculteurs sont inquiets. Cette saison, l'herbe n'a pas assez poussé. En Ardenne, on observe des volumes de la première coupe en baisse de 20 %. En Famenne, cela peut chuter de moitié. Si le fourrage est riche en sucre, il est particulièrement pauvre en protéines, à cause de la sécheresse. Les éleveurs vont devoir acheter des compléments alimentaires (soja, tourteaux de colza) et du fourrage pour leur bétail. Certains constituent déjà des dossiers en vue d'une indemnisation. Pour la deuxième coupe qui débute ces jours-ci, s'il se met à pleuvoir, on ne réglera pas le problème mais on sauvera les meubles", explique le chargé d'études Christian Hick.
  Afin de limiter un peu la casse, la FWA a introduit auprès du ministre wallon de l'Agriculture une demande d'autorisation de fauchage précoce des bandes herbagères, imposées par l'Union européenne afin de préserver la biodiversité. "René Collin nous a répondu qu'il devait interroger l'Europe et obtenir son accord", indique Christian Hick.
  "Concernant les conséquences de la sécheresse sur les grandes cultures, aucun constat ne peut être posé avant la récolte. On sait toutefois que la saison n'a pas démarré dans les meilleures conditions", signale le chargé d'études.

  Maladie venue d'ailleurs et insectes ravageurs
  .

  Depuis des années, la FWA conseille les agriculteurs sur la meilleure manière de faire face aux situations météorologiques difficiles, qui pourraient devenir la norme, et leurs corollaires : apparition depuis onze ans, sous nos latitudes (elle vient d'Afrique), de la maladie de la langue bleue qui peut tuer les ruminants, augmentation du nombre d'insectes ravageurs, des mauvaises herbes, besoins en eau accrus, faibles rendements agricoles, etc. "Des réponses existent comme la recherche de variétés de plantes plus résistantes (sans qu'elles soient des OGM), opter pour des espèces végétales en prairie moins gourmandes en eau et plus résilientes. Il y a aussi le travail fait par des producteurs/vendeurs de semences afin que ces nouvelles variétés soient disponibles pour les agriculteurs", conclut Christian Hick.
  Les agriculteurs wallons victimes d'un phénomène météorologique qui leur aura causé des dégâts et que l'IRM aura jugé exceptionnel (sécheresse, pluies incessantes, tempêtes, inondations... ) peuvent prétendre à une indemnisation via le Fonds des calamités agricoles.
  Depuis 2015, il est financé à hauteur de 4,5 millions d'euros par an par l'administration wallonne de l'Agriculture.
  Le ministre de l'Agriculture peut utiliser jusqu'à 1,7 million d'euros pour indemniser les agriculteurs sans en référer au gouvernement. Et il peut aussi utiliser en sus les réserves (les sommes non utilisées d'année en année) du Fonds. S'il a besoin de plus, le gouvernement peut accorder une dotation complémentaire puisée dans son budget. Et depuis ce 1er juin, la procédure de reconnaissance et d'indemnisation a été considérablement raccourcie et simplifiée.

  Une indispensable adaptation de l'agriculture
  .

  Avec le dérèglement climatique, le Fonds ne risque-t-il pas d'être beaucoup trop sollicité avec des conséquences budgétaires lourdes pour la Région wallonne ? "Ce n'est pas tant un problème budgétaire qu'une adaptation nécessaire de l'agriculture", souligne le ministre de l'Agriculture René Collin (CDH).
  "C'est un travail de longue haleine qui est en cours. Les agriculteurs sont demandeurs, partenaires et c'est dans leur intérêt car on observe déjà des changements : le rendement du blé qui stagne alors que celui du blé augmente, la floraison et la récolte des céréales plus précoce."
  Le ministre estime que la recherche apportera des solutions. "Les changements climatiques sont une des priorités assignées à l'outil de recherche public et je veux accélérer le tempo. Nous préparons d'ailleurs un appel à projets dans le cadre du programme triennal de recherche que nous finançons. Il y aura trois fils rouges : l'adaptation aux changements climatiques, la réduction des intrants (engrais et pesticides, NdlR) et la consolidation du revenu agricole, trois objectifs qui sont complémentaires."
  Plus largement, René Collin plaide pour que "politiquement on continue à se battre, jusqu'à l'échelon international, pour imposer une politique de lutte contre le réchauffement climatique".

  "Les assureurs sont dans leurs petits souliers"
  Coûteux. Sylvie Mélon gère depuis 15 ans un cabinet de courtage en assurances à Hannut. La moitié de sa clientèle est composée d'agriculteurs. "Il n'y a plus beaucoup de compagnies d'assurances qui les prennent en charge", précise-t-elle d'emblée. La grêle et les tempêtes sont couvertes par des contrats d'assurances agricoles mais pas la sécheresse. "Les primes sont variables, en fonction des zones à risque et c'est l'agriculteur qui décide du montant à assurer à l'hectare", explique Sylvie Mélon. Et ce n'est pas donné : pour l'escourgeon et les betteraves, c'est environ 2000 euros l'hectare, pour les pommes de terre, 4000 et pour les carottes, 5000. "Peu d'agriculteurs y souscrivent car c'est cher et certains renoncent à leur assurance après des années." Pourtant, le portefeuille d'assurances grêle de Sylvie Mélon "se maintient bien". En cas de sinistre, le dédommagement sera calculé sur le rendement moyen, tout en prenant compte du montant à l'hectare choisi par l'agriculteur. La directrice de Mélon Assurances estime que "les phénomènes de grêle, de mini-tornades vont se multiplier et survenir de façon très localisée. Je pense que les assureurs sont dans leurs petits souliers avec la crainte de calamités sur de grandes zones. On se regarde tous en chiens de faïence : va-t-on réduire ou augmenter l'offre ? A mon avis, dans les prochaines années, on ira plutôt vers une réduction de l'offre."
  L'Institut royal météorologique (IRM) qualifie la sécheresse en cours en Belgique de "préoccupante", au vu du déficit important de précipitations (moins de 60% de la normale saisonnière). "La situation pourrait devenir inquiétante pour plusieurs secteurs d'activités", annonce l'IRM. Selon ses prévisions, cet été devrait être plus chaud que la normale mais "aucune tendance particulière ne se dessine encore" en matière de précipitations. Il est difficile d'apprécier si la sécheresse actuelle, ou, au contraire, des excès de précipitations comme au printemps 2016 sont dus au réchauffement climatique. Des études conduites par Greenpeace, l'UCL ou Air Climat indiquent que la Belgique, à cause du réchauffement planétaire, connaîtra des étés plus chauds et secs, des hivers plus doux avec des épisodes de précipitations intenses d'ici à 2050. Le risque de tempêtes, d'inondations, d'érosion des sols sera accru. Rien de bon en vue pour les agriculteurs...
    
"""

print(jaccard_similarity(text_b, text_a))
print(lev.ratio(text_b, text_a))
