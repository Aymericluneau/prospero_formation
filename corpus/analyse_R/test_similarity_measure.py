import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata
import Levenshtein as lev
import numpy

PATH = os.path.abspath(os.path.split(__file__)[0])
print(PATH)
os.chdir(PATH)


def create_prc(df_keep):
    df_keep = df_keep.groupby(['Text_a']).size().reset_index(name='counts')
    print(len(df_keep))
    dossier_parent = os.path.dirname(PATH)
    liste_prc = []
    for element in os.listdir(dossier_parent) : # Pour chaque élément contenu dans le dossier parent
        adresse_element = f"{dossier_parent}\\{element}" #on recompose l'adresse de chque objet du dossier
        if adresse_element.endswith(".prc"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
            liste_prc.append(adresse_element)
        if os.path.isdir(adresse_element) : # si l'objet est un dossier, alors...
            for files in os.listdir(adresse_element): #on regarde chaque objet du dossier
                adresse_files = f"{adresse_element}\\{files}"
                if adresse_files.endswith(".prc"): #si l'objet est un fichier .prc on l'ajoute à la liste_prc
                    liste_prc.append(adresse_files)
    if len(liste_prc) > 1:
        print(f"Quel est le projet d'origine ?")
        for x, i in enumerate(liste_prc):
            print(f"[{x+1}] {i}")
        which_file = input("Entrez le nombre correspondant au fichier souhaité : ")
        print(f" Le fichier choisi est le {which_file}")
        prc_to_read = liste_prc[int(which_file) - 1]
    else:
        prc_to_read = liste_prc[0]
    with open(prc_to_read,"r", newline='', encoding = 'mbcs') as prc_file:
        read_prc = prc_file.readlines()
    name_prc = prc_to_read.split('\\')[-1]
    print(name_prc)
    new_path = prc_to_read.replace(name_prc, "")
    new_prc = f"{new_path[0:-1]}\\cleaned_score_prc.prc"
    with open(new_prc,"w", newline='', encoding = 'mbcs') as cleaned_prc:
        for i in read_prc[0:6]:
            cleaned_prc.write(i)
        for i in df_keep['Text_a']:
            cleaned_prc.write(f'{i}\n')
        cleaned_prc.write('ENDFILE')
    print(f"""Un nouveau fichier prc a été créé.
    Il est rangé à l'adress suivante : {new_prc}""")



def jaccard_similarity(text_a, text_b):
    a = set(text_a.split())
    b = set(text_b.split())
    c = a.intersection(b)
    jaccard_score = float(len(c)) / (len(a) + len(b) - len(c))
    return jaccard_score

def clean_duplicate_Nom(df_prc):
    list_size_file = []
    df_prc['Titre_sans_espaces'] = df_prc.Titre.str.lower().replace('[\s]', '', regex=True)
    for i in df_prc["Chemin"]:
        size_file = os.path.getsize(i)
        list_size_file.append(size_file)
    df_prc["Size_file"] = list_size_file
    df_prc = df_prc.sort_values(by=["Titre_sans_espaces", 'Auteur', 'Date', "Size_file"], ascending=[True, True, True, False])
    df_prc = df_prc.drop_duplicates(subset=['Titre_sans_espaces', 'Auteur', 'Date'], keep='first', inplace=False)
    df_prc = df_prc.sort_index()
    df_prc.to_csv("df_without_duplicates.csv", sep = '\t', index = False, encoding = 'utf-8')
    measure_distance(df_prc)

def measure_distance(df_prc):
    list_text_a = []
    list_text_a2 = []
    list_text_b = []
    list_textes_exclus = []
    list_score = []
    list_score2 = []
    n_texts = len(df_prc["Chemin"])
    for x, i in enumerate(df_prc["Chemin"]):
        if i not in list_textes_exclus:
            with open(i,"r", newline='', encoding = 'mbcs') as txt_file:
                text_a = txt_file.read()
            text_to_compare = df_prc.loc[x+1:, "Chemin"]
            for y, j in enumerate(text_to_compare):
                if j not in list_textes_exclus:
                    #print(f'le texte {x+1} est comparé au texte {x+y+2}')
                    with open(j,"r", newline='', encoding = 'mbcs') as txt_file2:
                        text_b = txt_file2.read()
                    score = jaccard_similarity(text_a, text_b)
                    if score > 0.95:
                        list_text_a2.append(i)
                        list_textes_exclus.append(j)
                        list_score2.append(score)
                    else:
                        list_text_a.append(i)
                        list_text_b.append(j)
                        list_score.append(score)


            compteur = list(str(x))
            if compteur[-1] == "0":
                print(f"text {x+1}")
                data = {"Text_a" : list_text_a, "Text_b" : list_text_b, "Jaccard" : list_score}
                data_exclues = {"Text_a" : list_text_a2, "Text_b" : list_textes_exclus, "Jaccard" : list_score2}
                df_keep= pandas.DataFrame(data)
                df_exclus= pandas.DataFrame(data_exclues)
                df_keep.to_csv("Liste_score_jaccard.csv", sep = '\t', index = False, encoding = 'utf-8')
                df_exclus.to_csv("Liste_score_jaccard2.csv", sep = '\t', index = False, encoding = 'utf-8')

    create_prc(df_keep)
            #

#Fichierz de départ
liste_fichiers = ["Liste_fichiers_PRC.csv", "Liste_score_jaccard.csv"]

find_fichier = any(liste_fichiers[1] in file for file in os.listdir(PATH))
if find_fichier == True:
    which_action = input("""Une liste des textes sans doublons (définie à partir des scores de similarité de Jaccard) existe déjà.
    Que souhaitez-vous faire ?
    [1] Créer un fichier .prc à partir de cette liste existante de textes
    [2] Recalculer les scores de similarité et créer un fichier .prc à partir des nouveaux scores
    """)
    if which_action == "1":
        df_keep = pandas.read_csv(liste_fichiers[1], sep = '\t') #on lit le fichier des textes conservés
        create_prc(df_keep)
    else:
        df_prc = pandas.read_csv(liste_fichiers[0], sep = '\t') #on lit le fichier des textes du prc de départ
        df_prc
        clean_duplicate_Nom(df_prc)

else:
    print("""Il n'existe pas de liste de textes sans doublons.
    Le programme va donc en créer une à partir des scores de
    similarité de Jaccard.""")
    df_prc = pandas.read_csv(liste_fichiers[0], sep = '\t')
    clean_duplicate_Nom(df_prc)


#jaccard_similarity(text_1, text_2)

#print(lev.ratio(text_1, text_2))
