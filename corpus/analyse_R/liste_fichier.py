#!/usr/bin/env python
# -*- coding: utf-8 -*-import sys

import os
from unidecode import unidecode
import re
import pandas
import string
import codecs
#import logging
#import csv
import unicodedata

PATH = os.path.abspath(os.path.split(__file__)[0])
os.chdir(PATH)

#Fonction pour récupérer les data in CTX file
def get_CTX_data(df):
    print("Fonction pour récupérer les data in CTX file")
    liste_titre = []
    liste_auteur = []
    liste_date = []
    liste_type = []
    liste_support = []
    lieu_emission = []
    parti_politique = []
    for x, address_file in enumerate(df['Chemin']):
        ctx_path = os.path.abspath(address_file.replace('.txt', '.ctx')) #remplace la terminaison '.txt' par l'extension '.ctx'
        if os.path.exists(ctx_path):
            with open(ctx_path,"r", newline='', encoding = 'mbcs') as ctx_file: #on ouvre le fichier CTX
                read_ctx = ctx_file.read()
            titre = read_ctx.split("\n")[1].strip() #On récupère le titre
            auteur = read_ctx.split("\n")[2].strip() #L'auteur
            date = read_ctx.split("\n")[5].strip() #Et la date
            support = read_ctx.split("\n")[6].strip()
            type_support = read_ctx.split("\n")[7].strip()
            lieu = read_ctx.split("\n")[10].strip()
            partis = read_ctx.split("\n")[11].strip()
            #print(partis)
            liste_titre.append(titre)
            liste_auteur.append(auteur)
            liste_date.append(date)
            liste_type.append(type_support)
            liste_support.append(support)
            lieu_emission.append(lieu)
            parti_politique.append(partis)
        else:
            print(f"Le fichier {ctx_path} n'existe pas")
    #Création de nouvelles colonnes
    df['Titre'] = liste_titre
    df['Auteur'] = liste_auteur
    df['Date'] = liste_date
    df['Support'] = liste_support
    df['Type_support'] = liste_type
    df['Lieu_emission'] = lieu_emission
    df['Parti_politique'] = parti_politique
    try:
        print(df)
    except:
        print("je n'ai pas pu imprimer le df")
    try:
        df_output = df.to_csv(liste_fichiers, sep = '\t', index = False) #on écrit les résultats dans le fichier
    except:
        print("La création du fichier n'a pas marcher")


def create_liste_fichiers(dossier_parent):
    liste_prc = []
    for root, dirs, files in os.walk(dossier_parent):
        for name in files:
            if ".prc" in name: #si l'objet est un fichier .prc on l'ajoute à la
                adresse_element = os.path.join(root, name)
                liste_prc.append(adresse_element)
            elif ".PRC" in name :
                adresse_element = os.path.join(root, name)
                liste_prc.append(adresse_element)

    if len(liste_prc) > 1:
        print(f"Il existe {len(liste_prc)} fichiers .prc, lequel voulez-vous utilisez ?")
        for x, i in enumerate(liste_prc):
            print(f"[{x+1}] {i}")
        which_file = input("Entrez le nombre correspondant au fichier souhaité : ")
        print(f" Le fichier choisi est le {which_file}")
        prc_to_read = liste_prc[int(which_file) - 1]
    else:
        prc_to_read = liste_prc[0]
    with open(prc_to_read,"r", newline='', encoding = 'mbcs') as prc_file:
        read_prc = prc_file.readlines()
    read_prc = read_prc[6:len(read_prc)-1]
    liste_chemin = []
    liste_text = []
    liste_nom_texte = []
    liste_size_text = []
    for chemin in read_prc:
        chemin = chemin.strip()
        nom_texte = chemin.split("\\")[-1]
        size_text_byte = os.path.getsize(chemin)
        size_text_page = (0.66*size_text_byte)/1325
        liste_chemin.append(chemin)
        liste_nom_texte.append(nom_texte)
        liste_size_text.append(size_text_page)
        with open(chemin,"r", newline='', encoding = 'mbcs') as text_file:
            read_text = text_file.read()
        liste_text.append(read_text)
    data = {"name" : liste_nom_texte,
    "Chemin" :liste_chemin,
    "Size" : liste_size_text,
    "Text" : liste_text
    }
    df = pandas.DataFrame (data, columns = ['name', 'Chemin', 'Size', 'Text'], index = None)
    #df = pandas.DataFrame (data, columns = ['name', 'Chemin', 'Size'], index = None)
    print(df)
    get_CTX_data(df)

def choix_du_dossier_parent():
    choix_dossier_parent = input(f"Entrez le chemin du dossier dans lequel vous souhaitez appliquer la recherche : \n")
    if os.path.isdir(choix_dossier_parent) == True:
        dossier = os.path.abspath(choix_dossier_parent)
        return dossier
    else:
        choix_dossier_parent = input("Le chemin n'existe pas. Entrez le nom d'un dossier existant : ")
        if os.path.isdir(choix_dossier_parent) == True:
            dossier = os.path.abspath(choix_dossier_parent)
            return dossier
        else:
            choix_dossier_parent = input("Le chemin n'existe pas. Le script va maintenant être fermé.")

def choix_du_dossier_courant():
    #dossier_courant = os.path.dirname(PATH)
    dossier_courant = PATH
    print(f"""Ce script sert à copier un fichier ".prc" de votre choix dans lequel sont répertoriées
    les adresses des textes constituant votre corpus, ainsi que celle des dictionnaires utilisés.
    Pour commencer, il vous faut choisir le dossier dans lequel seront recherchés les fichiers ".prc"
    Actuellement, le dossier courant (le dossier dans lequel vous avez placé le script) chemin est :\n
    {dossier_courant}\n""")
    choix_dossier_courant = input(f"""Souhaitez-vous rechercher les fichiers ".prc" présents dans le dossier courant
    ou faire la recherche dans un autre dossier ?
    [1] Oui, je souhaite rechercher les fichiers ".prc" présents dans le dossier courant : {dossier_courant}
    [2] Non, je souhaite rechercher les fichiers ".prc" présents dans le dossier parent : {os.path.dirname(PATH)}
    [3] Non, je souhaite rechercher les fichiers ".prc" présents dans un autre dossier (l'adresse du dossier devra être renseignée à l'étape suivante)
    Entrez le chiffre correspondant à votre choix : """)
    if choix_dossier_courant == "1":
        dossier_parent = dossier_courant
    elif choix_dossier_courant == "2":
        dossier_parent = os.path.dirname(PATH)
    elif choix_dossier_courant == "3":
        dossier_parent = choix_du_dossier_parent()
        print(dossier_parent)
    try:
        create_liste_fichiers(dossier_parent)
    except:
        s = input("""Le script n'a pas fonctionné. Probablement parce qu'il n'a pas trouvé de fichier ".prc" dans le dossier indiqué""")



#Fichier de départ
liste_fichiers = os.path.join(PATH, "Liste_fichiers_PRC.csv") #nom du fichier csv contenant la liste des textes basé sur le PRC
choix_du_dossier_courant()
