# Get the full power of Prospéro

Voir le tutoriel d'installation : http://aymericluneau.frama.io/prospero_formation

Ce dépôt framagit est un bookdown consacrée à l'installation et la prise en main de Prospéro I (en attendant Prospéro III).

D'après le modèle de https://rlesur.gitlab.io/bookdown-gitlab-pages

This repository is a bookdown focused on the implementation of Prospéro I. It is based on https://rlesur.gitlab.io/bookdown-gitlab-pages
